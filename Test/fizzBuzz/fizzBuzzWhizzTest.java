package fizzBuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class fizzBuzzWhizzTest {

	FizzBuzzWhizz fizzApp;
	@Before
	public void setUp() throws Exception {
		fizzApp = new FizzBuzzWhizz();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	// R0 - Function accepts number greater than 0
	@Test
	public void testFunctionAcceptsNumberGreaterThanZero() {
		assertEquals("error",  fizzApp.fbw(-1));
	}
}
